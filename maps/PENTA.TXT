
======================================================================
Title                 : Doomsday Shrine
Filename              : PENTA.MAP
Author                : J. Gareth "Praetor PredAtor" Moreton
E-mail                : gareth@garjen.co.uk
Web Page              : http://www.garjen.co.uk
Misc. Author Info     : TeamDUKE MAP
Other Levels          : Twin Forts
                        Steel Works
                        Bypassing Vessels
                        Water Cisterns
                        "Queen"
                        Polaris Moonbase

Description           : A mysterious and religious shrine with a DOOM
                        feeling to it (i.e. the pentagrams)

Additional Credits To : N/A
                          
======================================================================

* Play Information *

Episode and Level #    : N/A
Single Player          : No
DukeMatch 2-8 Player   : No
Cooperative 2-8 Player : No
Difficulty Settings    : No
2 Teamed CTF           : Yes
TeamDUKEMatch          : No
Enhanced DUKE-TAG      : No
FLAG-TAG               : Yes
Plutonium Pak Required : Requires eDuke
New Art                : No
New Music              : No
New Sound Effects      : Yes
New Sprite Palettes    : Yes
New .CON Files         : TEAMDUKE.CON
Demos Replaced         : None

=====================================================================

* Construction *

Base                   : New level from scratch
Level Editor(s) Used   : MAPSTER 0.39� by Elite Productions Inc.
                         (e-mail "terminx@theoffspring.net")
Art Editor(s) Used     : EDITART (Though I didn't use it for this particular
                         map)
Construction Time      : 48 Hours (additional 6 hours for TROR conversion)
Known Bugs/Problems    : The central platform sometimes suffers from
                         graphical glitches related to True Room-over-Room

* Where to get this MAP file *

File location          : Garjen Web Site and perhaps some other
                         DUKE3D sites.
=====================================================================

*Important Information*

Installation           : Not for the actual map, but there is
                         installation required for the latest
                         version of TeamDUKE (version 0.61�)

Important Notes        : TeamDUKE should only be downloaded from
                         the Garjen Web Site.  As for the MAP, it can be
                         distributed on your own web site, as long as
                         this file comes with it and neither this file
                         nor the MAP are modified in any way.

                         TeamDUKE was created in 2000-2023 by
                         Garjen Software.
======================================================================
