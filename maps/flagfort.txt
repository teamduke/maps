
======================================================================
Title                 : Twin Forts
Filename              : FLAGFORT.MAP
Author                : J. Gareth "Praetor PredAtor" Moreton
E-mail                : gareth@garjen.co.uk
Web Page              : http://www.garjen.co.uk
Misc. Author Info     : TeamDUKE MAP (Requires v0.6�)
Other Levels          : Steel Works
                        Bypassing Vessels
                        Water Cisterns
                        "Queen"

Description           : Two mediaeval castles now converted for
                        DUKE-CTF use.  Many would like to use bows
                        'n arrows.  Sorry Doc, no such weapons
                        allowed.  Feet, pistols, shotguns,
                        rippers, RPGs, Pipe Bombs, Shrinkers,
                        Expanders, Devastators, Trip Bombs and
                        Freezethrowers only!

Additional Credits To : Peter M. "Plugwash" Green
                          
======================================================================

* Play Information *

Episode and Level #    : Episode 2, Level 1 (In TeamDUKE CONS)
Single Player          : No
DukeMatch 2-8 Player   : No
Cooperative 2-8 Player : No
Difficulty Settings    : No
2 Teamed DUKE-CTF      : Yes
TeamDUKEMatch          : Yes
DUKE-TAG               : No
Plutonium Pak Required : Requires eDuke
New Art                : Yes
New Music              : No
New Sound Effects      : Yes
New .CON Files         : GJCTF.CON
Demos Replaced         : Previous versions of the MAP did exist on
                         Garjen

=====================================================================

* Construction *

Base                   : CASTLE.MAP - An early DukeMatch(TM) map by
                         Praetor PredAtor - Available on Garjen.
Level Editor(s) Used   : BUILD
Art Editor(s) Used     : EDITART
Construction Time      : Wasn't recorded
Known Bugs/Problems    : There were a few problems regarding
                         overlapping sectors.  Some still exist in
                         the level at certain positions/angles, but
                         Dukes will be too busy playing CTF to
                         notice.

* Where to get this MAP file *

File location          : Garjen Web Site
=====================================================================

*Important Information*

Installation           : Unzip into your DUKE directory.
                         Installation is required for TeamDUKE.

Important Notes        : This MAP can be uploaded to your own site,
                         as long as this file is shipped with it, and
                         that neither the MAP nor this file are modified.
                         Also, it must only be distributed FREELY.  It
                         must NOT be distributed for any sort of profit.
                         (except popularity if that counts)
                         
                         TeamDUKE should only be downloaded from
                         the Garjen Web Site - if found elsewhere
                         consult me immediately.

                         TeamDUKE was created in 2000-2001 by
                         Garjen Software Ltd.
======================================================================
